all: invtest
debug:
		make C=ifortDEBUG
build:
		make C=ifort
build_fast:
		make C=ifort_fast


BUILD_DIR=
LIB_DIR=


FOPT= -O3 -132
COPT= -03

ifeq ($(C),ifort)

else ifeq ($(C),ifort_fast)
FOPT= -O3 -132 -parallel -xHost -ipo -fast
else ifeq ($(C),ifortDEBUG)
FOPT= -O0 -132 -traceback -O0 -fstack-protector -check all  -assume realloc_lhs -ftrapuv -fpe0 -warn -traceback -debug extended
COPT= -00 -O0 -Wall -g
endif

CC=gcc
CCFLAGS=-c $(COPT)
FF=ifort
FCFLAGS=-c  -Iinclude $(USED_LIBRARY) $(FOPT)
LDFLAGS=$(LIBS)  -mkl -lmkl_lapack95_lp64 -lmkl_blas95_lp64 -lmkl_intel_lp64 -L${MKLROOT}/lib/intel64



F90_SOURCES= refsor.f90 \
		uniinv.f90 \
		unirnk.f90 \
		unista.f90 \
		knitinv.f90


F90_OBJECTS=$(F90_SOURCES:%.f90=%.o)
BUILD_F90_OBJECTS=$(patsubst %.o,$(BUILD_DIR)%.o,$(F90_OBJECTS))


EXECUTABLE=invtest


invtest: main.f90 $(F90_OBJECTS)
		$(FF) main.f90 $(FOPT) -I$(BUILD_DIR) $(BUILD_F90_OBJECTS) $(LDFLAGS) -o $@

.f90.o:
		$(FF) $(FCFLAGS) $< -o $@


refsor.o: refsor.f90
	$(FF) $(FCFLAGS) refsor.f90 -o $(BUILD_DIR)$@

uniinv.o: uniinv.f90
	$(FF) $(FCFLAGS) uniinv.f90 -o $(BUILD_DIR)$@

unirnk.o: unirnk.f90
	$(FF) $(FCFLAGS) unirnk.f90 -o $(BUILD_DIR)$@

unista.o: unista.f90
	$(FF) $(FCFLAGS) unista.f90 -o $(BUILD_DIR)$@

knitinv.o: knitinv.f90
	$(FF) $(FCFLAGS) knitinv.f90 -o $(BUILD_DIR)$@


clean:
		rm -f $(BUILD_DIR)*.o $(BUILD_DIR)*.mod *.txt fort.* 2> /dev/null


dlib:
		ifort -shared -fpic -Iinclude  $(F90_SOURCES)  -o $(LIB_DIR)libknitinv.so

slib:
		ar rcs $(LIB_DIR)libknitinv.a $(BUILD_DIR)*.o
