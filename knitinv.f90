! ------------------------------------------------------------ !
! Finding selected elements of the inverse of sparse matrix.
! Algoritm is not optimal for the perfomance but in case of large number
! or needed elements one may divide the task on multiple CPUs.
!
! Implementation based on the Ref. "Knitting algorithm for calculating
! Green functions in quantum systems".
! * K. Kazymyrenko and X. Waintal, Phys. Rev. B 77, 115119
! How to use: see test file.
! 1. Call selinv%init_memory(no_nodes,no_vals,vals,rc,no_inc)
! 2. and find inverses for selected elements: call selinv%invert(elements,evals)
! ------------------------------------------------------------ !
module modknitinv
use m_unista
use m_refsor
use m_unirnk
implicit none
private
integer, parameter:: dprec=kind(0.d0)
real(kind=dprec),parameter :: numerical_zero = 1.0D-16
integer :: KNITINV_DEBUG = 0 ! show 1, hide 0 debug information


ENUM, BIND(C)
  ENUMERATOR :: KITINV_TIME_INT_ST_1   = 1
  ENUMERATOR :: KITINV_TIME_INT_ST_2   = 2
  ENUMERATOR :: KITINV_TIME_ELE_ST_1   = 3
  ENUMERATOR :: KITINV_TIME_ELE_ST_2   = 4
  ENUMERATOR :: KITINV_TIME_REINDEX    = 5
  ENUMERATOR :: KITINV_TIME_TOTAL      = 6
END ENUM
real(kind=dprec) :: knitinv_profiler(KITINV_TIME_TOTAL)
! -----------------------------------------------------------
! Structure which holds the value of the n-th element in the
! matrix and the coupling values to neightbours. This type
! is generated from the rows(),cols(),values() information of
! the matrix.
! -----------------------------------------------------------
type node
    complex(kind=dprec),allocatable :: Tij(:) ! hoping from current i-th element to neightboors T(i->j)
    complex(kind=dprec),allocatable :: Tji(:) ! inverse of above T(j->i)
    integer   ,allocatable :: Iij(:)          ! global indices of neightbours
    integer   ,allocatable :: Iij_less(:)     ! global indices of neightbours with id smaller than current node
    complex(kind=dprec) :: H0                 ! on site value
    integer    :: no_bonds                    ! number of hopings, size of the Tij,Tji, and Iij vectors
    integer    :: no_used                     ! number of connections in dynamic interface (used in algorithm)
    integer    :: fl_id                       ! dynamically contains mapping from global id to local id in the interface (here called frontline)
    integer    :: no_bonds_less               ! number of neightbours with id smaller than current node id. Size of Iij_less
    contains
    procedure,pass(this) :: reset_node ! reset variables and deallocate arrays
    procedure,pass(this) :: alloc_mem  ! allocate all arrays
end type node

! -----------------------------------------------------------
! Main type responsible for the selected inversion of the
! sparse matrix. Contains structure of the matrix represented
! by nodes and seleted elements to be inversed.
! -----------------------------------------------------------
type knitinv
    type(node) , allocatable    :: nodes(:) ! node representation of the inversed matrix
    integer                     :: no_nodes ! size of the matrix, and number of points to be inversed
    integer                     :: no_inc
    contains
    procedure,pass(this) :: init_memory ! initialize matrix structure
    procedure,pass(this) :: free_memory ! free memory
    procedure,pass(this) :: invert      ! find selected elements of inverse of a matrix.
end type knitinv

! -----------------------------------------------------------
! Make public some of the variables
! -----------------------------------------------------------
public :: KNITINV_DEBUG
public :: knitinv,knit_clock,knit_invmat ! auxiliary functions
public :: knit_unique_elements
contains


real function knit_clock() result(c)
    INTEGER :: clock_rate,c_time
    CALL SYSTEM_CLOCK(COUNT_RATE=clock_rate)
    CALL SYSTEM_CLOCK(COUNT=c_time)
    c = (real(c_time))/clock_rate
end function knit_clock


! -----------------------------------------------------------
!                           KnitInv
! Free all allocated memory: matrix and selected points.
! -----------------------------------------------------------
subroutine free_memory(this)
    class(knitinv) :: this
    integer :: i
    ! deallocate matrix
    do i = 1 , this%no_nodes
        call this%nodes(i)%reset_node()
    enddo
    if(allocated(this%nodes))deallocate(this%nodes)
    ! just in case deallocate selected points, they
    ! should be deallocated in invert function
    this%no_nodes    = 0
end subroutine free_memory


! ------------------------------------------------------------------------
!                           KnitInv
! Initialize memory, and convert matrix from row-col representation
! to arrays of nodes.
! no_nodes - number of nodes, i.e. size of the matrix.
! no_vals  - lenght of the array vals(:) and rc(:,1:2)
! vals(:)  - array which contains non-zero elements of the matrix
! rc(:,row/col)- contains row=1 and col=2 id to non zero values of matrix.
! no_inc   - number of added sites per step must be > 0, for some matrices
!            different value of no_inc may lead to better results.
! ------------------------------------------------------------------------
subroutine init_memory(this,no_nodes,no_vals,vals,rc,no_inc)
    class(knitinv) :: this
    integer       :: no_nodes,no_vals
    complex(kind=dprec)    :: vals(:)
    integer       :: rc(:,:),no_inc
    ! local variables
    integer    :: i,r,c,k

    ! Free memory before initializating another one
    call this%free_memory()
    this%no_nodes = no_nodes
    !if(no_inc /= 1) then
    r = mod(no_inc-mod(no_nodes,no_inc),no_inc)
    this%no_nodes = no_nodes + r
    this%no_inc   = no_inc
    !endif
    if(KNITINV_DEBUG > 0)then
        print*,"knitinv::matrix initialization"
        print*,"         matrix sparsity:",no_vals/dble(no_nodes**2)
    endif

    allocate(this%nodes(this%no_nodes))

    ! clear memory of nodes
    do i = 1 , this%no_nodes
        call this%nodes(i)%reset_node()
    enddo
    ! caclulate number of hopings for each row/node
    do i = 1 , no_vals
        r = rc(i,1)
        c = rc(i,2)
        if(r /= c) this%nodes(r)%no_bonds = this%nodes(r)%no_bonds + 1
    enddo
    ! reallocate memory to calculated number of bonds in previous step
    ! alloc_mem uses this%nodes(i)%no_bonds to allocate memory
    do i = 1 , no_nodes
        call this%nodes(i)%alloc_mem()
    enddo
    do i = no_nodes + 1 , this%no_nodes
        this%nodes(i)%no_bonds = 0
        this%nodes(i)%H0       = 1
    enddo

    ! Fill matrices using provided arrays
    do i = 1 , no_vals
        r = rc(i,1)
        c = rc(i,2)
        if( r == c ) then
            this%nodes(r)%H0 = vals(i)
        else
            this%nodes(r)%no_used = this%nodes(r)%no_used + 1
            k = this%nodes(r)%no_used
            this%nodes(r)%Tij(k) = vals(i)
            this%nodes(r)%Iij(k) = c
        endif
    enddo
    ! Fill the Tji vectors, this increases the number of memory
    ! usage but in the main loop one does not have to search for
    ! the Tij coupling.
    do i = 1 , no_nodes
        this%nodes(i)%no_used = 0
        do k = 1 , this%nodes(i)%no_bonds
            r = this%nodes(i)%Iij(k)
            do c = 1 , this%nodes(r)%no_bonds
                if( this%nodes(r)%Iij(c) == i ) exit
            enddo
            this%nodes(i)%Tji(k) = this%nodes(r)%Tij(c)
        enddo
    enddo
    ! Calculate number of neightboors with id smaller than current node id.
    do i = 1 , no_nodes
        this%nodes(i)%no_bonds_less = 0

        do k = 1 , this%nodes(i)%no_bonds
            if(this%nodes(i)%Iij(k) < i) this%nodes(i)%no_bonds_less = this%nodes(i)%no_bonds_less + 1
        enddo
        deallocate(this%nodes(i)%Iij_less)
        if(this%nodes(i)%no_bonds_less > 0) allocate(this%nodes(i)%Iij_less(this%nodes(i)%no_bonds_less))
    enddo
    ! Fill Iij_less array.
    do i = 1 , no_nodes
        r = 0
        if(this%nodes(i)%no_bonds_less /= 0) then
        do k = 1 , this%nodes(i)%no_bonds
            if(this%nodes(i)%Iij(k) < i) then
                r = r + 1
                this%nodes(i)%Iij_less(r) = this%nodes(i)%Iij(k)
            endif
        enddo
        endif
    enddo
end subroutine init_memory

! -------------------------------------------------------------- !
!
! -------------------------------------------------------------- !
subroutine reset_node(this)
    class(node) :: this
    this%no_bonds      = 0
    this%no_used       = 0
    this%no_bonds_less = 0
    this%fl_id         = 0
    this%H0            = 0
    if(allocated(this%Iij))      deallocate(this%Iij)
    if(allocated(this%Tij))      deallocate(this%Tij)
    if(allocated(this%Tji))      deallocate(this%Tji)
    if(allocated(this%Iij_less)) deallocate(this%Iij_less)
endsubroutine reset_node


! -------------------------------------------------------------- !
!
! -------------------------------------------------------------- !
subroutine alloc_mem(this)
    class(node) :: this
    allocate(this%Iij(this%no_bonds))
    allocate(this%Iij_less(this%no_bonds_less))
    allocate(this%Tij(this%no_bonds))
    allocate(this%Tji(this%no_bonds))
    this%Iij      = 0
    if(this%no_bonds_less > 0) this%Iij_less = 0
    this%Tij = 0
    this%Tji = 0
endsubroutine alloc_mem

! -----------------------------------------------------------
!                           KnitInv
! Find the selected elements of the inverse of the input matrix.
! elements(no_elements,2) - array which contains pairs (row,col)
!               to elements which are suppose to be inverted
! values(no_elements) - contains values of selected elements.
!
! call init_memory before this function to allocate memory.
! -----------------------------------------------------------
subroutine invert(this,elements,values)
    class(knitinv)                    :: this
    integer,dimension(:,:)           :: elements
    complex(kind=dprec),dimension(:) :: values

    ! internal variables:
    integer             :: no_inc
    doubleprecision     :: progress,progress_it
    integer             :: r,q,p,k,l,i,j,s1,s2
    integer             :: c_node , max_node , min_node
    integer             :: fl_size
    complex(kind=dprec) :: ctmp1,ctmp2,t_tmp

    ! number of auxiliary matrices need to compute elements
    complex(kind=dprec) ,allocatable ::   HAA    (:,:),  GAA    (:,:),invGAA(:,:)
    complex(kind=dprec) ,allocatable :: TauFA    (:,:),TauAF    (:,:),GFF   (:,:),nGFF(:,:)
    complex(kind=dprec) ,allocatable :: TauFA_GAA(:,:),GAA_TauAF(:,:)
    complex(kind=dprec) ,allocatable :: small_TauFA_GAA(:,:),small_GAA_TauAF(:,:)
    complex(kind=dprec) ,allocatable :: Gas(:,:),Gsa(:,:),Ga_A(:,:),GA_b(:,:)

    integer,allocatable :: id_HAA(:),coupled_ids(:),lg_coupled_ids(:)

    integer,allocatable :: fl_lg(:),new_fl_lg(:)   ! global IDs for old and new frontline
    integer,allocatable :: l_id_GFF(:),l_id_GAA(:) ! mappings from new frontline to old
    integer             :: l_id_GFF_size           ! new frontline size without added sites
    integer             :: l_id_GAA_size           ! new ftronline size of sites which belongs to added sites
    integer             :: fl_no_sites             ! total frontline size


    integer :: c_elem,no_elems,iTmp,small_coupling
    logical :: eTest

    ! matrices used to compute seleted elements
    complex(kind=dprec),allocatable :: row_oGij(:,:),row_nGij(:,:)
    complex(kind=dprec),allocatable :: col_oGij(:,:),col_nGij(:,:)
    complex(kind=dprec),allocatable :: row_Ga_A(:,:),col_GA_b(:,:)

    complex(kind=dprec),allocatable :: tmpMat(:,:),vecA(:),vecB(:)
    complex(kind=dprec),allocatable :: e_Gp(:)
    complex(kind=dprec),allocatable :: cGxs(:,:),cGsx(:,:),eGa_A(:,:),eGA_b(:,:)
    complex(kind=dprec),allocatable :: small_cGxs(:,:),small_cGsx(:,:)

    integer              :: no_row_elems,no_col_elems
    integer ,allocatable :: row_elems(:), col_elems(:) , itmp_array(:)
    integer ,allocatable :: map_row_elems(:), map_col_elems(:)

    no_elems = size(elements,1)
    no_inc   = this%no_inc


    allocate(e_Gp(no_elems ))
    allocate(cGxs(1,no_inc))
    allocate(eGa_A(1,no_inc))
    allocate(cGsx(no_inc,1))
    allocate(eGA_b(no_inc,1))
    allocate(vecA(no_inc))
    allocate(vecB(no_inc))

    ! Generating list of unique rows and cols i.e user may ask to
    ! calculate e.g. following elements (1,2) , (1,1) , (2,1) , (2,2)
    ! note that there are only two unique values for rows and cols
    ! (1,2) for cols and (1,2) for rows. Removing duplicates
    ! improves speed of calculation. To remove duplicates we use
    ! orderpack+ 2.0 functions.
    allocate(map_row_elems(no_elems))
    allocate(map_col_elems(no_elems))
    allocate(itmp_array(no_elems))

    map_row_elems = elements(:,1)
    map_col_elems = elements(:,2)

    ! find number of unique elements in rows and cols
    call unista(map_row_elems,no_row_elems)
    call unista(map_col_elems,no_col_elems)

    allocate(row_elems(no_row_elems))
    allocate(col_elems(no_col_elems))
    ! generate arrays containg only the unique values


    call unirnk(map_row_elems,row_elems,no_row_elems)
    call unirnk(map_col_elems,col_elems,no_col_elems)


    do i = 1,no_row_elems
        itmp_array(i) = map_row_elems(row_elems(i))
    enddo
    row_elems = itmp_array(1:no_row_elems)
    do i = 1,no_col_elems
        itmp_array(i) = map_col_elems(col_elems(i))
    enddo
    col_elems = itmp_array(1:no_col_elems)

    ! create mapping from global element index to array
    ! of unique values.
    map_row_elems = 0
    map_col_elems = 0

    do i = 1 , no_elems
        s1 = elements(i,1)
        s2 = elements(i,2)
        do j = 1 , no_row_elems
            if( row_elems(j) == s1 ) then
                map_row_elems(i) = j
                exit
            endif
        enddo
        do j = 1 , no_col_elems
            if( col_elems(j) == s2 ) then
                map_col_elems(i) = j
                exit
            endif
        enddo
    enddo

    allocate(row_nGij(0,no_row_elems))
    allocate(row_oGij(0,no_row_elems))
    allocate(col_nGij(0,no_col_elems))
    allocate(col_oGij(0,no_col_elems))
    allocate(row_Ga_A(no_inc,no_row_elems))
    allocate(col_GA_b(no_inc,no_col_elems))
    row_Ga_A = 0
    col_GA_b = 0
    row_oGij = 0
    col_oGij = 0
    allocate(HAA(no_inc,no_inc),GAA(no_inc,no_inc),invGAA(no_inc,no_inc))
    allocate(id_HAA(this%no_nodes))

    ! dynamically changing arrays
    allocate(TauFA(1,1))
    allocate(TauAF(1,1))
    allocate(GFF(0,0))
    allocate(nGFF(0,0))
    allocate(l_id_GFF(0))
    allocate(l_id_GAA(0))
    allocate(fl_lg(0))
    allocate(new_fl_lg(0))
    allocate(TauFA_GAA(0,0))
    allocate(GAA_TauAF(0,0))
    allocate(small_TauFA_GAA(0,0))
    allocate(small_GAA_TauAF(0,0))
    allocate(small_cGsx(0,0))
    allocate(small_cGxs(0,0))
    allocate(Gas(0,0))
    allocate(Gsa(0,0))
    allocate(Ga_A(0,0))
    allocate(GA_b(0,0))
    allocate(tmpMat(0,0))
    allocate(coupled_ids(0))
    allocate(lg_coupled_ids(0))

    ! Set starting values:
    l_id_GFF_size = 0
    l_id_GAA_size = 0
    fl_no_sites   = 0
    id_HAA        = 0
    fl_size       = 0
    e_Gp          = 0
    progress_it   = 0
    ! Reset nodes values, in case of multiple calls of invert function
    do c_node = 1 , this%no_nodes
        this%nodes(c_node)%fl_id   = 0
        this%nodes(c_node)%no_used = 0
    enddo
    knitinv_profiler = 0
    knitinv_profiler(KITINV_TIME_TOTAL) = knit_clock()

    if(KNITINV_DEBUG > 0) then
        print*,"knitinv::starting"
        print*,"         Num. elems. ............",no_elems
        print*,"         Unique row/col elems ...",no_row_elems,no_col_elems
        print*,"         Matrix size ............",this%no_nodes
        print*,"         Step size ..............",no_inc
    endif
    ! ---------------------------------------------------------------------
    ! Start main iteration:
    ! ---------------------------------------------------------------------
    do c_node = 1 , this%no_nodes , no_inc
        ! calulate first and last id of added site
        min_node = c_node
        max_node = c_node + no_inc - 1
        ! timer
        progress = c_node/(this%no_nodes+0.0)
        if(progress > progress_it) then
            progress_it = progress_it + 0.05
            if(KNITINV_DEBUG > 0) then
                print"(A,I3,A)"," knitinv::solving stage:",nint(progress*100),"%"
            endif
        endif
        t_tmp = knit_clock()

        ! reallocate matrices:
        deallocate(TauFA,TauFA_GAA)
        deallocate(TauAF,GAA_TauAF)
        deallocate(coupled_ids)
        ! Coupling matrices between frontline and added sites
        allocate(TauFA    (fl_size,no_inc))
        allocate(TauAF    (no_inc,fl_size))
        allocate(TauFA_GAA(fl_size,no_inc))
        allocate(GAA_TauAF(no_inc,fl_size)) ! aux matrix

        ! aux matrices
        deallocate(cGsx,cGxs)
        allocate(cGxs(1,fl_size))
        allocate(cGsx(fl_size,1))

        ! coupled_ids - hold 1 for sites from [A-1] which are in contact to added sites [A]
        allocate(coupled_ids(fl_size))

        HAA   = 0
        TauAF = 0
        TauFA = 0
        p     = 0
        ! calculate mapping from global id to local ids in added sites
        do k = min_node , max_node
            p         = p + 1
            id_HAA(k) = p
        enddo
        ! calulate Hamiltonian matrix H0 of added sites and coupling
        ! matrices, between H0 and old frontline [A-1]
        p = 0
        coupled_ids = 0
        do k = min_node , max_node
             p        = p + 1
             HAA(p,p) = this%nodes(k)%H0
             do q = 1 , this%nodes(k)%no_bonds
                l = this%nodes(k)%Iij(q)
                r = id_HAA(l)
                if(r /= 0) then
                    HAA(p,r) = this%nodes(k)%Tij(q)
                endif
                s1= this%nodes(l)%fl_id
                if( s1 /= 0 ) then
                    TauAF(p,s1) = this%nodes(k)%Tij(q)
                    TauFA(s1,p) = this%nodes(k)%Tji(q)
                    coupled_ids(s1) = 1
                endif
             enddo
        enddo

        small_coupling = sum(coupled_ids) ! number of really connected sites

        deallocate(small_TauFA_GAA,small_GAA_TauAF)
        deallocate(small_cGsx,small_cGxs,lg_coupled_ids)

        allocate(small_TauFA_GAA(small_coupling,no_inc))
        allocate(small_GAA_TauAF(no_inc,small_coupling))

        allocate(small_cGsx(small_coupling,1))
        allocate(small_cGxs(1,small_coupling))
        allocate(lg_coupled_ids(small_coupling))

        ! create mapping to really connected sites
        s1 = 0
        do i = 1 , fl_size
            if(coupled_ids(i) == 1) then
                s1 = s1 + 1
                lg_coupled_ids(s1) = i
            endif
        enddo


        ! Actualize the number of coupling on the frontline after addition
        ! of new site.
        do k = min_node , max_node
            do s1 = 1 , this%nodes(k)%no_bonds_less
                p = this%nodes(k)%Iij_less(s1)
                this%nodes(p)%no_used = this%nodes(p)%no_used + 1
                this%nodes(k)%no_used = this%nodes(k)%no_used + 1
            enddo
        enddo

        ! calculate the Greens function of added sites [A]
        ! from Eq. (8)
        invGAA = HAA - matmul(TauAF,matmul(GFF,TauFA))
        GAA    = invGAA
        call knit_invmat(no_inc,GAA) ! inverse this matrix

        ! precalulate GAA * TauAF and TauFA * GAA
        call K_ZGEMM(GAA,TauAF,GAA_TauAF)
        call K_ZGEMM(TauFA,GAA,TauFA_GAA)
        ! create smaller matrices based on the list of coupled
        ! sites
        do i = 1 , small_coupling
           j = lg_coupled_ids(i)
           small_TauFA_GAA(i,:) = TauFA_GAA(j,:)
           small_GAA_TauAF(:,i) = GAA_TauAF(:,j)
        enddo


        knitinv_profiler(KITINV_TIME_INT_ST_1)  &
        = knitinv_profiler(KITINV_TIME_INT_ST_1) + knit_clock() - t_tmp
        t_tmp               = knit_clock()

        deallocate(tmpMat)
        allocate(tmpMat(1,no_inc))


        ! -------------------------------------------------------------
        ! Updates rows from Eq. (9)
        ! -------------------------------------------------------------
        do c_elem = 1 , no_row_elems
            if(row_elems(c_elem) > max_node) cycle ! skip rows which will give zero values
            ctmp1 = -1
            eTest = .false.
            ! Find if current element belongs to added sites
            do k = min_node , max_node
                if(row_elems(c_elem) == k) then
                    eTest = .true.
                    iTmp  = k - c_node + 1
                    exit
                endif
            enddo
            if( eTest  )then ! when row belongs to added site use already calculated Gaa
                row_Ga_A(:,c_elem) = Gaa(iTmp,:)
                row_oGij(:,c_elem) = 0
            else ! otherwise use Eq. (9)
                do i = 1 , small_coupling
                   j = lg_coupled_ids(i)
                   small_cGxs(1,i) = row_oGij(j,c_elem)
                enddo
                do i = 1 , no_inc
                    eGa_A(1,i) = -sum(small_cGxs(1,:)*small_TauFA_GAA(:,i))
                enddo
                row_Ga_A(:,c_elem) =  eGa_A(1,:)
            endif
        enddo ! end of loop over elements
        ! -------------------------------------------------------------
        ! Same as above but for cols -- Eq. (10)
        ! -------------------------------------------------------------
        do c_elem = 1 , no_col_elems
            if(col_elems(c_elem) > max_node) cycle
            eTest = .false.
            do k = min_node , max_node
                if(col_elems(c_elem) == k) then
                    eTest = .true.
                    iTmp  = k - c_node + 1
                    exit
                endif
            enddo
            if( eTest  )then
                col_GA_b(:,c_elem) = Gaa(:,iTmp)
                col_oGij(:,c_elem) = 0
            else ! normal treatement
                do i = 1 , small_coupling
                   j = lg_coupled_ids(i)
                   small_cGsx(i,1) = col_oGij(j,c_elem)
                enddo
                do i = 1 , no_inc
                    eGA_b(i,1) = -sum(small_cGsx(:,1)*small_GAA_TauAF(i,:))
                enddo
                col_GA_b(:,c_elem) = eGA_b(:,1)
            endif
        enddo ! end of loop over elements

        ! -------------------------------------------------------------
        ! Updates elements values from Eq. (11)
        ! -------------------------------------------------------------
        do c_elem = 1 , no_elems
            if(elements(c_elem,1) > max_node) cycle
            if(elements(c_elem,2) > max_node) cycle

            eGa_A(1,:) = row_Ga_A(:,map_row_elems(c_elem))
            eGA_b(:,1) = col_GA_b(:,map_col_elems(c_elem))

            ! From Eq. (11)
            ctmp1 = 0
            do j = 1 , no_inc
                ctmp2 = 0
            do i = 1 , no_inc
                ctmp2 = ctmp2 + eGa_A(1,i)*invGaa(i,j)
            enddo
                ctmp1 = ctmp1 + ctmp2*eGA_b(j,1)
            enddo
            e_Gp (c_elem)   = e_Gp (c_elem) + ctmp1

        enddo ! end of loop over elements


        knitinv_profiler(KITINV_TIME_ELE_ST_1)  &
        = knitinv_profiler(KITINV_TIME_ELE_ST_1) + knit_clock() - t_tmp
        t_tmp               = knit_clock()
        ! -----------------------------------------------
        ! Creating new frontline:
        ! -----------------------------------------------
        q = 0
        do p = 1 , fl_size
            ! Count only those sites which number of used
            ! connection is smaller than number of bounds
            if(this%nodes(fl_lg(p))%no_used < this%nodes(fl_lg(p))%no_bonds )then
                q = q + 1
            endif
        enddo
        r = 0
        do k = min_node , max_node
            if(this%nodes(k)%no_used < this%nodes(k)%no_bonds) r = r + 1
        enddo

        l_id_GFF_size = q
        l_id_GAA_size = r


        if(r + q == 0) exit ! this will exit for the last element where frontline is closed

        deallocate(l_id_GAA,l_id_GFF,new_fl_lg)
        allocate  (l_id_GAA(l_id_GAA_size),l_id_GFF(l_id_GFF_size))
        allocate  (new_fl_lg(l_id_GAA_size+l_id_GFF_size))

        ! Create new frontline for elements without added sites l_id_GFF
        q = 0
        do p = 1, fl_size
            if(this%nodes(fl_lg(p))%no_used < this%nodes(fl_lg(p))%no_bonds )then
                q            = q + 1
                new_fl_lg(q) = fl_lg(p)
                l_id_GFF(q)  = p
            endif
        enddo
        ! Add new site to the frontline, but keep it in separate array: l_id_GAA
        r = 0
        do k = min_node , max_node
            if(this%nodes(k)%no_used < this%nodes(k)%no_bonds) then
                q = q + 1
                r = r + 1
                new_fl_lg(q) = k
                l_id_GAA(r)  = id_HAA(k)
            endif
        enddo

        knitinv_profiler(KITINV_TIME_REINDEX)  &
        = knitinv_profiler(KITINV_TIME_REINDEX) + knit_clock() - t_tmp
        t_tmp               = knit_clock()


        deallocate(nGFF,Gas,Gsa,Ga_A,GA_b,tmpMat)
        p = l_id_GAA_size+l_id_GFF_size
        allocate(nGFF(p,p))
        allocate(Gas(l_id_GFF_size,fl_size))
        allocate(Gsa(fl_size,l_id_GFF_size))
        allocate(Ga_A(l_id_GFF_size,no_inc))
        allocate(GA_b(no_inc,l_id_GFF_size))
        allocate(tmpMat(no_inc,l_id_GFF_size))

        ! Update the Greens function of the new frontline from Eq. (9-11)
        do i = 1 , l_id_GFF_size
            Gas(i,:) = GFF(l_id_GFF(i),:)
            Gsa(:,i) = GFF(:,l_id_GFF(i))
        enddo
        ! Eq. (9-10)
        ctmp1 = -1
        call K_ZGEMM(Gas,TauFA_GAA,Ga_A,alpha=ctmp1)
        call K_ZGEMM(GAA_TauAF,Gsa,GA_b,alpha=ctmp1)

        ! Eq. (11) copy old values from frontline Greens function
        do j = 1 , l_id_GFF_size
        do i = 1 , l_id_GFF_size
            nGFF(i,j) = GFF(l_id_GFF(i),l_id_GFF(j))
        enddo
        enddo

        ! Eq. (11) calculate second term
        p = l_id_GFF_size
        call K_ZGEMM(invGAA,GA_b,tmpMat)
        ctmp1 = 1
        call K_ZGEMM(Ga_A,tmpMat,nGFF(1:p,1:p),beta=ctmp1)

        ! In case when new frontline belongs to added sites use previously
        ! calculated Greens functions: Gaa from Eq. (8)
        do i = 1 , l_id_GAA_size
        do j = 1 , l_id_GAA_size
            nGFF(p+i,p+j) = GAA(l_id_GAA(i),l_id_GAA(j))
        enddo
        enddo
        ! Same as above but for GaA and GAb elements
        do i = 1 , l_id_GFF_size
            do j = 1 , l_id_GAA_size
                nGFF(i,p+j) = Ga_A(i,l_id_GAA(j))
                nGFF(p+j,i) = GA_b(l_id_GAA(j),i)
            enddo
        enddo


        knitinv_profiler(KITINV_TIME_INT_ST_2)  &
        = knitinv_profiler(KITINV_TIME_INT_ST_2) + knit_clock() - t_tmp
        t_tmp               = knit_clock()


        ! ---------------------------------------------------
        ! Updating Greens function of selected Rows and Cols
        ! ---------------------------------------------------
        deallocate(Gas,Gsa)
        p = l_id_GAA_size+l_id_GFF_size
        allocate(Gas(no_inc,p))
        allocate(Gsa(no_inc,p))

        do i = 1 , l_id_GFF_size
        do j = 1 , no_inc
            Gas(j,i) = GA_b(j,i)
            Gsa(j,i) = Ga_A(i,j)
        enddo
        enddo
        q = l_id_GFF_size
        do i = 1 , l_id_GAA_size
        do j = 1 , no_inc
            Gas(j,i+q) = GAA(j,l_id_GAA(i))
            Gsa(j,i+q) = GAA(l_id_GAA(i),j)
        enddo
        enddo

       p = l_id_GAA_size+l_id_GFF_size

        if(p /= size(col_nGij,1))then
            deallocate(col_nGij)
            deallocate(row_nGij)
            allocate(col_nGij(p,no_elems))
            allocate(row_nGij(p,no_elems))
        endif

        deallocate(tmpMat,cGxs)
        allocate(tmpMat(1,l_id_GFF_size))
        allocate(cGxs(l_id_GFF_size,1))

        r = l_id_GFF_size

        ! -------------------------------------------------------------
        ! Rows -- Eq. (11)
        ! -------------------------------------------------------------
        do c_elem = 1 , no_row_elems
            if(row_elems(c_elem) > max_node) cycle
            do i = 1 , r
                row_nGij(i,c_elem) = row_oGij(l_id_GFF(i),c_elem)
            enddo
            do i = 1 , no_inc
                eGa_A(1,i) = sum(row_Ga_A(:,c_elem)*invGaa(:,i))
            enddo
            do i = 1 , r
                row_nGij(i,c_elem) = row_nGij(i,c_elem) + sum(eGa_A(1,:)*Gas(:,i))
            enddo
            do i = 1 , l_id_GAA_size
                row_nGij(r+i,c_elem) = row_Ga_A(l_id_GAA(i),c_elem)
            enddo
        enddo
        ! -------------------------------------------------------------
        ! Cols -- Eq. (11)
        ! -------------------------------------------------------------
        do c_elem = 1 , no_col_elems
            if(col_elems(c_elem) > max_node) cycle
            do i = 1 , r
                col_nGij(i,c_elem) = col_oGij(l_id_GFF(i),c_elem)
            enddo
            call K_ZGEMM(invGaa,col_GA_b(:,c_elem:c_elem),eGA_b)
            do i = 1 , r
                col_nGij(i,c_elem) = col_nGij(i,c_elem) + sum(Gsa(:,i)*eGA_b(:,1))
            enddo
            do i = 1 , l_id_GAA_size
                col_nGij(r+i,c_elem) = col_GA_b(l_id_GAA(i),c_elem)
            enddo
        enddo
        ! copy new values to old arrays.

        p = l_id_GAA_size+l_id_GFF_size
        if(p /= size(col_oGij,1))then
            deallocate(col_oGij)
            deallocate(row_oGij)
            allocate(col_oGij(p,no_elems))
            allocate(row_oGij(p,no_elems))
        endif
        col_oGij = col_nGij
        row_oGij = row_nGij


        knitinv_profiler(KITINV_TIME_ELE_ST_2)  &
        = knitinv_profiler(KITINV_TIME_ELE_ST_2) + knit_clock() - t_tmp
        t_tmp               = knit_clock()


        ! -------------------------------------------------------------
        ! Update global to local indexing of new frontline: fl_id
        ! -------------------------------------------------------------

        do p = 1, fl_size
            this%nodes(fl_lg(p))%fl_id     = 0
        enddo

        fl_size = l_id_GAA_size + l_id_GFF_size
        deallocate(GFF,fl_lg)
        allocate(GFF(fl_size,fl_size),fl_lg(fl_size))
        GFF   = nGFF
        fl_lg = new_fl_lg


        do p = 1, fl_size
            this%nodes(new_fl_lg(p))%fl_id = p
        enddo

        do k = min_node , max_node
            id_HAA(k) = 0
        enddo
        knitinv_profiler(KITINV_TIME_REINDEX)  &
        = knitinv_profiler(KITINV_TIME_REINDEX) + knit_clock() - t_tmp

    enddo ! end of loop c_node

    knitinv_profiler(KITINV_TIME_TOTAL) = knit_clock() - knitinv_profiler(KITINV_TIME_TOTAL)

    do c_elem = 1 , no_elems
        values(c_elem) = e_Gp(c_elem)
    enddo

    if(KNITINV_DEBUG > 0) then
        print*,"knitinv::Profilling info calculation times for selected stages:"
        print*,"    Step 1. for interface:",knitinv_profiler(KITINV_TIME_INT_ST_1) ,"[s]"
        print*,"    Step 1. for elements :",knitinv_profiler(KITINV_TIME_ELE_ST_1) ,"[s]"
        print*,"    Step 2. for interface:",knitinv_profiler(KITINV_TIME_INT_ST_2) ,"[s]"
        print*,"    Step 2. for elements :",knitinv_profiler(KITINV_TIME_ELE_ST_2) ,"[s]"
        print*,"    Renumbering nodes    :",knitinv_profiler(KITINV_TIME_REINDEX) ,"[s]"
        print*,"    Total time           :",knitinv_profiler(KITINV_TIME_TOTAL) ,"[s]"
    endif
    if(KNITINV_DEBUG > 0) then
        print*,"knitinv::finished"
    endif


    deallocate(e_Gp)
    deallocate(cGxs)
    deallocate(eGa_A)
    deallocate(cGsx)
    deallocate(eGA_b)
    deallocate(vecA)
    deallocate(vecB)
    deallocate(map_row_elems)
    deallocate(map_col_elems)
    deallocate(itmp_array)
    deallocate(row_nGij)
    deallocate(row_oGij)
    deallocate(col_nGij)
    deallocate(col_oGij)
    deallocate(row_Ga_A)
    deallocate(col_GA_b)
    deallocate(HAA,GAA,invGAA)
    deallocate(id_HAA)
    deallocate(TauFA)
    deallocate(TauAF)
    deallocate(GFF)
    deallocate(nGFF)
    deallocate(l_id_GFF)
    deallocate(l_id_GAA)
    deallocate(fl_lg)
    deallocate(new_fl_lg)
    deallocate(TauFA_GAA)
    deallocate(GAA_TauAF)
    deallocate(small_TauFA_GAA)
    deallocate(small_GAA_TauAF)
    deallocate(small_cGsx)
    deallocate(small_cGxs)
    deallocate(Gas)
    deallocate(Gsa)
    deallocate(Ga_A)
    deallocate(GA_b)
    deallocate(tmpMat)
    deallocate(coupled_ids)
    deallocate(lg_coupled_ids)

end subroutine invert

SUBROUTINE K_ZGEMM(A,B,C,TRANSA,TRANSB,ALPHA,BETA)
    ! Fortran77 call:
    ! ZGEMM(TRANSA,TRANSB,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)

    CHARACTER(LEN=1), INTENT(IN), OPTIONAL :: TRANSA
    CHARACTER(LEN=1), INTENT(IN), OPTIONAL :: TRANSB
    COMPLEX(kind=dprec), INTENT(IN), OPTIONAL :: ALPHA
    COMPLEX(kind=dprec), INTENT(IN), OPTIONAL :: BETA
    COMPLEX(kind=dprec), INTENT(IN)    :: A(:,:)
    COMPLEX(kind=dprec), INTENT(IN)    :: B(:,:)
    COMPLEX(kind=dprec), INTENT(INOUT) :: C(:,:)

    integer :: M,N,K,LDA,LDB,LDC
    CHARACTER(LEN=1) :: p_TRANSA
    CHARACTER(LEN=1) :: p_TRANSB
    COMPLEX(kind=dprec):: p_ALPHA
    COMPLEX(kind=dprec):: p_BETA


    M = size(A,1)
    N = size(B,2)
    K = size(A,2)

    if(M == 0 .or. N == 0 .or. K == 0 ) return

    LDA = M
    LDB = size(B,1)
    LDC = size(C,1)

    p_TRANSA = 'N'
    p_TRANSB = 'N'
    p_ALPHA  = 1.0
    p_BETA   = 0.0

    if(present(TRANSA)) p_TRANSA = TRANSA
    if(present(TRANSB)) p_TRANSB = TRANSB
    if(present(ALPHA))  p_ALPHA  = ALPHA
    if(present(BETA))   p_BETA   = BETA

    call ZGEMM(p_TRANSA,p_TRANSB,M,N,K,p_ALPHA,A,LDA,B,LDB,p_BETA,C,LDC)

END SUBROUTINE K_ZGEMM


subroutine knit_invmat(N,A)
  integer :: N
  complex*16,dimension(:,:):: A
  complex*16,allocatable,dimension(:)  :: WORK
  integer,allocatable,dimension (:)    :: IPIV
  integer info,error

  allocate(WORK(N),IPIV(N),stat=error)
  if (error.ne.0)then
    print *,"ZGETRF::error:not enough memory"
    stop
  end if
  call ZGETRF(N,N,A,N,IPIV,info)
  if(info .eq. 0) then
!    write(*,*)"succeded"
  else
    write(*,*)"ZGETRF::failed with info:",info
    write(*,*)"           It seems your matrix is singular check your matrix"
  end if
  call ZGETRI(N,A,N,IPIV,WORK,N,info)
  if(info .eq. 0) then
!    write(*,*)"succeded"
  else
   write(*,*)"ZGETRI::failed with info:",info
  end if
  deallocate(IPIV,WORK,stat=error)
  if (error.ne.0)then
    print *,"ZGETRF::error:fail to release"
    stop
  end if
end subroutine knit_invmat

subroutine knit_unique_elements(elements,unique_elems)
    integer :: elements(:,:)
    integer,allocatable :: unique_elems(:)

    integer :: no_row_elems,no_col_elems,no_elems
    integer,allocatable,dimension(:) :: map_row_elems,map_col_elems,itmp_array
    integer,allocatable,dimension(:) :: row_elems,col_elems
    integer :: i
    no_elems = size(elements,1)

    allocate(map_row_elems(no_elems))
    allocate(map_col_elems(no_elems))


    map_row_elems = elements(:,1)
    map_col_elems = elements(:,2)
    ! find number of unique elements in rows and cols
    call unista(map_row_elems,no_row_elems)
    call unista(map_col_elems,no_col_elems)

    allocate(row_elems(no_row_elems))
    allocate(col_elems(no_col_elems))

    ! generate arrays containg only the unique values
    call unirnk(map_row_elems,row_elems,no_row_elems)
    call unirnk(map_col_elems,col_elems,no_col_elems)

    allocate(itmp_array(no_row_elems+no_col_elems))

    do i = 1,no_row_elems
        itmp_array(i) = map_row_elems(row_elems(i))
    enddo

    do i = 1,no_col_elems
        itmp_array(i+no_row_elems) = map_col_elems(col_elems(i))
    enddo
    call unista(itmp_array,no_elems)

    if(allocated(unique_elems)) deallocate(unique_elems)
    allocate(unique_elems(no_elems))
    deallocate(col_elems)
    allocate(col_elems(no_elems))

    call unirnk(itmp_array,col_elems,no_elems)

    do i = 1,no_elems
        unique_elems(i) = itmp_array(col_elems(i))
    enddo

    deallocate(row_elems)
    deallocate(col_elems)
    deallocate(itmp_array)
    deallocate(map_row_elems)
    deallocate(map_col_elems)

end subroutine knit_unique_elements

end module modknitinv
