program knitinv_test
use modknitinv
use ifport
implicit none

integer,parameter :: msize = 3000 , no_elems = 1000
complex*16        :: vals(msize*msize),rMat(msize,msize),evals(no_elems)
integer           :: rc(msize*msize,2),elements(no_elems,2)
type(knitinv)     :: tknitinv

integer         :: i,j,no_nonzero,k
doubleprecision :: sum_delta,delta


print*,"Generate some random and complex matrix..."
rMat = 0
call srand(100)
do i = 1 , msize
    j = 1
    do while(j<=msize/4)
        rMat(i,j) = 1.0+cmplx(rand(),rand())
        rMat(j,i) = 1.0-cmplx(rand(),rand())
        j = j + rand()*5
    enddo
enddo
do i = 1 , msize
    rMat(i,i) = 1.0*i
enddo

print*,"Converting to ROW-COL-VAL format"
no_nonzero = 0
do i = 1, msize
do j = 1, msize
    if(abs(rMat(i,j)) > 0.00001D0 ) then
        no_nonzero = no_nonzero + 1
        vals(no_nonzero) = rMat(i,j)
        rc(no_nonzero,1) = i
        rc(no_nonzero,2) = j
    endif
enddo
enddo

print*,"Direct inverse of generated matrix:"
call knit_invmat(msize,rMat)

print*,"Generating random elements to calculate:"
KNITINV_DEBUG = 1
do i = 1 , no_elems
    elements(i,:) = (/NINT(rand()*(msize-1))+1,NINT(rand()*(msize-1))+1/)
enddo
call tknitinv%init_memory(msize,no_nonzero,vals,rc,60)
call tknitinv%invert(elements,evals)
call tknitinv%free_memory()

sum_delta = 0
do i = 1 , size(evals)
    delta = abs(evals(i) - rMat(elements(i,1),elements(i,2)))
    sum_delta = sum_delta + delta
enddo
print*,"Total error:",sum_delta
end program knitinv_test
